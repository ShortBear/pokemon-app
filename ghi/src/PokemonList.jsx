import PokemonCard from './PokemonCard';
import { useSelector } from 'react-redux';
import { useGetAllPokemonQuery } from './app/apiSlice';

const PokemonList = () => {
    const searchCriteria = useSelector(state => state.search.value);
    const { data, isLoading } = useGetAllPokemonQuery();

    if (isLoading) return <>Loading...</>

    const filteredData = () => {
        if (searchCriteria) return data.filter(pokemon => pokemon.name.includes(searchCriteria))
        return data;
    }

    return (
        <>
            <h1 className='mt-3'>
                Pokemon{' '}
                {searchCriteria && <small className='text-body-secondary'>"{searchCriteria}"</small>}
            </h1>
            <div className="row mt-3">
                {filteredData().map(p => <PokemonCard key={p.name} name={p.name} />)}
            </div>
        </>
    )
}

export default PokemonList;
